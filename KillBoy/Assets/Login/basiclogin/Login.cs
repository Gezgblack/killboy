﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class Login : MonoBehaviour {
    #region Vars
    public static string Email = "";
    public static string Password = "";
    string loginstatus = "";
    string registerstatus = "";

    private string CreateAccountUrl = "http://killboydatabase.000webhostapp.com/CreateAccount.php";
    private string LoginUrl = "http://killboydatabase.000webhostapp.com/LoginAccount.php";
    private string ConfirmPass = "";
    private string ConfirmEmail = "";
    //Creation Vars
    private string CEmail = "";
    private string CPassword = "";

    public InputField em;
    public InputField pas;
    public Button loginbtn;
    public InputField cem;
    public InputField cpas;
    public InputField confirmCem;
    public InputField conrimCpas;
    public GameObject panelA = new GameObject("Panel");
    public GameObject panelB = new GameObject("Panel");
    public GameObject backDoor = new GameObject("Panel"); //backdoor
    private bool show = false;
    public Text loginStatus;
    public Text registerStatus;
    public Text registerSuccess;


    //GUI
    public string currentMenu = "Login";
    #endregion

    void Start()
    {
        PlayerPrefs.DeleteKey("email");
        PlayerPrefs.DeleteKey("gameDifficulty");
        PlayerPrefs.DeleteKey("playerScore");
    }

    void Update()
    {

        if (Input.GetKeyDown(KeyCode.RightControl) || Input.GetKeyDown(KeyCode.F4)) //escape button
        {
            show = !show;
        }

        if (show)
        {
            backDoor.SetActive(true); //show
        }

        if (!show)
        {
            backDoor.SetActive(false); //hide
        }
    }

    public void PressedLoginFromMainMenu()
    {
        StartCoroutine(LoginAccount());
    }

    public void PressedCreateAccountFromRegister()
    {
        if (cem.text == confirmCem.text && cpas.text == conrimCpas.text)
        {
            StartCoroutine(CreateAccount());
            cem.text = "";
            cpas.text = "";
            confirmCem.text = "";
            conrimCpas.text = "";

        }
        else if(conrimCpas.text != cpas.text && cem!= confirmCem)
        {
            registerStatus.text= "Password and Email did not match";
        }
        else if(cpas.text != conrimCpas.text)
        {
            registerStatus.text = "Password did not match";
        }
        else if(cem.text != confirmCem.text)
        {
            registerStatus.text = "Email did not match";
        }
    }

    public void PressRegisterFromMainMenu()
    {
        panelB.SetActive(true);
        panelA.SetActive(false);
    }

    public void PressBackFromRegister()
    {
        panelB.SetActive(false);
        panelA.SetActive(true);
    }

    public void UserChecker(WWW w)
    {
        if(w.text=="Connection Succesfuluser not found")
        {
            Debug.Log("User not found");
            loginStatus.text = "User Not Found";
        }
        else if(w.text=="Connection Succesfullogin success")
        {
            Debug.Log("Success Login");
            PlayerPrefs.SetString("email", em.text);
            SceneManager.LoadScene(2);
            Debug.Log(PlayerPrefs.GetString("email"));
            
        }
        else if (w.text == "Connection SuccesfulPassword incorrect")
        {
            Debug.Log("Password Incorrect");
            loginStatus.text = "Password or Email Incorrect";
        }
        else if (w.text == "Connection SuccesfulAlreadyUsed")
        {
            Debug.Log("Email Already Registered");
            registerStatus.text = "Email Already Used";
        }
        else if (w.text == "Connection SuccesfulSuccess")
        {
            Debug.Log("Created new User");
            registerStatus.text = "";
            registerSuccess.text = "User Created";
        }
        else
        {
            Debug.Log("ERROR in UserChecker()");
        }
    }

    //The following code is for the purpose of a back door for the login, incase the database goes down during the demo
    public void shamil()
    {
        PlayerPrefs.SetString("email", "shamil");
        SceneManager.LoadScene(2);
    }

    public void martin()
    {
        PlayerPrefs.SetString("email", "martin");
        SceneManager.LoadScene(2);
    }

    public void john()
    {
        PlayerPrefs.SetString("email", "john");
        SceneManager.LoadScene(2);
    }


    #region Coroutines

    IEnumerator CreateAccount()
    {
        //Sending messages to php script
        Debug.Log("button pressed");
        WWWForm Form = new WWWForm();

        CEmail = cem.text; //get text from the field
        Form.AddField("emailPost", CEmail);
        CPassword = cpas.text; //get tect from the field
        Form.AddField("passwordPost", CPassword);

        WWW CreateAccountWWW = new WWW(CreateAccountUrl, Form);
        // Wait for the php to send a response
        yield return CreateAccountWWW;
        UserChecker(CreateAccountWWW);
        if (CreateAccountWWW.error != null)
        {
            Debug.LogError("Cannot Connect to Account Creation");
            Debug.Log(CreateAccountWWW.error);
        }
        else
        {
            Debug.Log(CreateAccountWWW.text);
            string CreateAccountReturn = CreateAccountWWW.text;
            if (CreateAccountReturn == "Success")
            {
                Debug.Log("Success: Account created");
                currentMenu = "Login";
            }
        }
    }

    IEnumerator LoginAccount()
    {
        WWWForm Form = new WWWForm();
        Email = em.text;
        Form.AddField("emailPost", Email);
        Password = pas.text;
        Form.AddField("passwordPost", Password);

        WWW www = new WWW(LoginUrl, Form);
        yield return www;

        Debug.Log(www.text);
        UserChecker(www);
    }

    #endregion
}
