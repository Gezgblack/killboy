﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class pauseMenu : MonoBehaviour { //Game Pause Controls

    public GameObject PauseUI; //pause panel
    public GameObject PlayerUI;

    private bool pause = false;

	// Use this for initialization
	void Start () {

        PauseUI.SetActive(false); //hide on start
	}
	
	// Update is called once per frame
	void Update () {

        if (Input.GetButtonDown("Pause")) //escape button
        {
            pause = !pause;
        }

        if (pause)
        {
            PauseUI.SetActive(true); //show
            Time.timeScale = 0;
            PlayerUI.SetActive(false);
        }

        if (!pause)
        {
            PauseUI.SetActive(false); //hide
            Time.timeScale = 1;
            PlayerUI.SetActive(true);
        }
	}

    public void Resume()
    {
        pause = false; //resume
    }

    public void Restart()
    {
        Application.LoadLevel(Application.loadedLevel); //current level
    }

    public void MainMenu()
    {
        SceneManager.LoadScene(2);
        pause = false;
    }

    public void Quit()
    {
        Application.Quit();
    }
}
