﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class displayHighScoresMultiplayer : MonoBehaviour {

    public Text[] highscoreText; // To access our text which displays the scores
    highScoresMultiplayer highscoreManager; // To manage our scores

    // Use this for initialization
    void Start()
    {

        for (int i = 0; i < highscoreText.Length; i++)
        {
            highscoreText[i].text = i + 1 + ". Fetching..."; // getting info from database status
        }
        highscoreManager = GetComponent<highScoresMultiplayer>();

        StartCoroutine("RefreshHighScores");

    }

    public void OnHighscoresDownloaded(highScoreMultiplayer[] highscoreList) // To set the username and score texts
    {
        for (int i = 0; i < highscoreText.Length; i++)
        {
            highscoreText[i].text = i + 1 + ". ";
            if (highscoreList.Length > i) //if we have more highscores text than actual high scores
            {
                highscoreText[i].text += highscoreList[i].gameName + "-" + highscoreList[i].score; //change the text to {username} - {score}
            }
        }
    }

    IEnumerator RefreshHighScores() // To download the new scores every 30 seconds
    {
        while (true)
        {
            highscoreManager.DownloadScores();
            yield return new WaitForSeconds(2);
        }
    }
}

