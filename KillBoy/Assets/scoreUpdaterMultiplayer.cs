﻿using UnityEngine;
using System.Collections;

public class scoreUpdaterMultiplayer : MonoBehaviour {

    highScoresMultiplayer hsm;

	// Use this for initialization
	void Start () {

        hsm = GetComponent<highScoresMultiplayer>();
        int score = PlayerPrefs.GetInt("waveScore");
        hsm.AddNewHighScore(PlayerPrefs.GetString("gameName"), score);
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
