﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityStandardAssets.Characters.FirstPerson;
using UnityEngine.Networking;

public class fireBulletMultiplayer : NetworkBehaviour
{ //defines how the weapon works

    public float timeBetweenBullets = 0.15f; //every 15 hundred of a second we can fire the gun, how often we can shoot(to avoid having a large stream of bullets being shot)

    public GameObject projectile; //what we can shoot, the bullet
    float nextBullet; //when the next bullet can be shot
    public FirstPersonController controller;
    private Slider ammoSlider;
    [SerializeField]
    private GameObject muzzle;

    //Bullet data
    public Slider playerAmmoSlider;
    public int maxRounds; //the max amount of bullets the weapon can have
    public int startingRounds; //the amount of bullets we start  
    [SyncVar(hook = "OnAmmoChanged")]
    public int remainingRounds; //rounds left

    //Sound
    AudioSource gunMuzzleAS;
    public AudioClip shootSound;
    public AudioClip reloadSound;

    //inventory info
    public Sprite weaponSprite; //the actual weapon image, that represents the weapon we are holding
    public Image weaponImage; //the location in the gui where the sprite is

    //money
    private Player_Shoot money;

    public override void OnStartLocalPlayer()
    {
        ammoSlider = GameObject.Find("playerAmmoSlider").GetComponent<Slider>(); //find our ammo slider
        ammoSlider.value = remainingRounds;

        money = GetComponent<Player_Shoot>();
    }

    // Use this for initialization
    void Awake()
    {

        nextBullet = 0; //no wait time, between each bullet
        remainingRounds = startingRounds; //the amount of rounds we start with
        //playerAmmoSlider.maxValue = maxRounds; //set slider to the max
        //playerAmmoSlider.value = remainingRounds; //the actual rounds in the slider value
        gunMuzzleAS = GetComponent<AudioSource>();
        controller = GetComponentInParent<FirstPersonController>();

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetAxisRaw("Fire1") > 0 && nextBullet < Time.time && remainingRounds > 0)
        {//if the person is actually shooting, and remaining rounds are greater then 0
            Shoot();
        }
    }

    [Command]
    void CmdOnShoot()
    {
        RpcDoShootEffect();

    }

    [ClientRpc]
    void RpcDoShootEffect()
    {

        nextBullet = Time.time + timeBetweenBullets;
        //Instantiate(projectile, transform.position, Quaternion.Euler(controller.transform.rotation.eulerAngles));
        playSound(shootSound); //play shoot sound

        remainingRounds -= 1; //with each bullet we shoot, delete 1 bullet
                              //playerAmmoSlider.value = remainingRounds; //update the amount of rounds we have after deletion

        Instantiate(projectile, muzzle.transform.position, Quaternion.Euler(controller.transform.rotation.eulerAngles));
    }
        
   

    [Client]
    void Shoot()
    {
        if (!isLocalPlayer)
        {
            return;
        }

        CmdOnShoot();
        ammoSlider.value = remainingRounds;
    }

    public void reload()
    {
        remainingRounds = maxRounds; //set remaining ammo to the max amount
        playerAmmoSlider.value = remainingRounds; //update the slider
        playSound(reloadSound); //play reload sound
    }

    void playSound(AudioClip playthesound) //play sound method
    {
        gunMuzzleAS.clip = playthesound; //associate sound
        gunMuzzleAS.Play(); //play the sound of shooting
    }

    public void initialializeWeapon()
    {
        //gunMuzzleAS.clip = reloadSound;
        //gunMuzzleAS.Play(); //weapon switch, reload sound play
        nextBullet = 0; //fire the weapon imediately
        //playerAmmoSlider.maxValue = maxRounds; //set slider to max
        //playerAmmoSlider.value = remainingRounds; //reflext the actual slider value (ammo value)
        weaponImage.sprite = weaponSprite; //change the sprite of th weapon
    }

    void OnAmmoChanged(int bullets)
    {
        remainingRounds = bullets;
    }

    /*ADD AMMO*/
    public void AddAmmo() //buying ammo
    {
        remainingRounds = maxRounds; //set remaining ammo to the max amount
        ammoSlider.value = remainingRounds; //update the slider
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Ammo" && remainingRounds < maxRounds) //&& money.playerMoney >= 5) //if he player layer is colliding with the collider and the ammo is not full 
        {
            AddAmmo(); //Buy the ammo
            money.playerMoney -= 5;  
        }
    }
}
