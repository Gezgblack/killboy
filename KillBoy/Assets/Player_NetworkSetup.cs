﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class Player_NetworkSetup : NetworkBehaviour
{
    [SerializeField]
    Camera FPSCharecterCam; //player camera
    [SerializeField]
    AudioListener audioListener; //player audio
    //[SerializeField]
    //fireBulletMultiplayer fireBullet;

    public override void OnStartLocalPlayer()
    {
        GameObject.Find("Scene Camera").SetActive(false);
        // Enable local player attributes
        GetComponent<CharacterController>().enabled = true;
        GetComponent<UnityStandardAssets.Characters.FirstPerson.FirstPersonController>().enabled = true;
        GetComponentInChildren<fireBulletMultiplayer>().enabled = true;
        FPSCharecterCam.enabled = true;
        audioListener.enabled = true;

        //syncing player animation
        GetComponent<NetworkAnimator>().SetParameterAutoSend(0, true); //local player
    }

    public override void PreStartClient() //client
    {
        GetComponent<NetworkAnimator>().SetParameterAutoSend(0, true);
    }
}
