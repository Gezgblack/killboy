﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class scoreManager : MonoBehaviour {

    public Text scoreText;
    public float scoreCount;
    public float pointsPerSecond;
    public bool scoreIncreasing;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        if (scoreIncreasing)
        {
            scoreCount -= pointsPerSecond * Time.deltaTime;
            //PlayerPrefs.SetFloat("saveScore", scoreCount);
            if(scoreCount <= 0)
            {
                Application.LoadLevel(Application.loadedLevel); //current level
            }
        }

        scoreText.text = "" + Mathf.Round(scoreCount);
	}
}
