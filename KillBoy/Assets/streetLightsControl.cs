﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class streetLightsControl : NetworkBehaviour {

    [SerializeField]
    public Light[] lights;

    [SerializeField]
    SpawnManager_waveLevel level;

    [SyncVar(hook = "OnStreetLightsChange")]
    public float lightAmout = 0f;

    public override void OnStartLocalPlayer()
    {
        OnStreetLightsChange(lightAmout);
    }

    void OnStreetLightsChange(float amount)
    {
        foreach(Light light in lights)
        {
            light.intensity = amount;
        }
    }
}
