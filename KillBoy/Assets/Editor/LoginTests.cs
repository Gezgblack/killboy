﻿using UnityEngine;
using UnityEditor;
using NUnit.Framework;

public class LoginTests {

	[Test]
	public void EditorTest()
	{
		//Arrange
		var gameObject = new GameObject();

		//Act
		//Try to rename the GameObject
		var newGameObjectName = "My game object";
		gameObject.name = newGameObjectName;

		//Assert
		//The object has a new name
		Assert.AreEqual(newGameObjectName, gameObject.name);
	}

    [Test]
    public void ClickingRegister_HideLoginPanel_PanelHides()
    {

        //Arrange
        var loginMenu = new Login();

        //Assert
        Assert.AreEqual(loginMenu.panelA.activeSelf, true);

        //Act
        loginMenu.PressRegisterFromMainMenu();

        //Assert
        Assert.AreEqual(loginMenu.panelA.activeSelf, false);
    }

    [Test]
    public void ClickingBackFromRegisterPanel_HideRegisterPanel_PanelHides()
    {

        //Arrange
        var loginMenu = new Login();

        //Assert
        Assert.AreEqual(loginMenu.panelB.activeSelf, true);

        //Act
        loginMenu.PressBackFromRegister();

        //Assert
        Assert.AreEqual(loginMenu.panelB.activeSelf, false);
    }
}
