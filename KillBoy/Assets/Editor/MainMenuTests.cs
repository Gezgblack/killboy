﻿using UnityEngine;
using UnityEditor;
using NUnit.Framework;
using System;

public class MainMenuTests {

	[Test]
	public void EditorTest()
	{
		//Arrange
		var gameObject = new GameObject();

		//Act
		//Try to rename the GameObject
		var newGameObjectName = "My game object";
		gameObject.name = newGameObjectName;

		//Assert
		//The object has a new name
		Assert.AreEqual(newGameObjectName, gameObject.name);
	}

    [Test]
    public void ClickingSinglePlayer_HideMenuPanel_PanelHides()
    {

        //Arrange
        var menu = new MainMenuActions();

        //Assert
        Assert.AreEqual(menu.panelMainMenu.activeSelf, true);

        //Act
        menu.singlePlayerPressedFromMainMenu();

        //Assert
        Assert.AreEqual(menu.panelMainMenu.activeSelf, false);
    }

    [Test]
    public void ClickingBackInSinglePlayer_HideSinglePlayerPanel_PanelHides()
    {

        //Arrange
        var menu = new MainMenuActions();

        //Assert
        Assert.AreEqual(menu.panelSingleplayer.activeSelf, true);

        //Act
        menu.backPressedFromSinglePlayer();

        //Assert
        Assert.AreEqual(menu.panelSingleplayer.activeSelf, false);
    }

    [Test]
    public void ClickingPlayInSinglePlayer_HideSinglePlayerPanel_PanelHides()
    {

        //Arrange
        var menu = new MainMenuActions();

        //Assert
        Assert.AreEqual(menu.panelSingleplayer.activeSelf, true);

        //Act
        menu.playPressedFromSinglePlayer();

        //Assert
        Assert.AreEqual(menu.panelSingleplayer.activeSelf, false);
    }

    [Test]
    public void ChoosingEasyDifficulty_DifficultyChosen_DifficultySetToEasy()
    {

        //Arrange
        var menu = new MainMenuActions();

        //Assert
        Assert.IsEmpty(menu.difficultyChooser);

        //Act
        menu.setToDifficultyToEasy();

        //Assert
        Assert.AreEqual(menu.difficultyChooser.ToString(), "Easy");
    }

    [Test]
    public void ChoosingMediumDifficulty_DifficultyChosen_DifficultySetToMedium()
    {

        //Arrange
        var menu = new MainMenuActions();

        //Assert
        Assert.IsEmpty(menu.difficultyChooser);

        //Act
        menu.setToDifficultyToNormal();

        //Assert
        Assert.AreEqual(menu.difficultyChooser.ToString(), "Normal");
    }
}
