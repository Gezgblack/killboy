﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class Zombie_Target : NetworkBehaviour {

    private UnityEngine.AI.NavMeshAgent agent;
    private Transform myTransform;
    public Transform targetTransform;
    private LayerMask raycastLayer;
    private float radius = 500; //the radius within detection
    private Animator myAnim;

	// Use this for initialization
	void Start () {
        //get all componenets
        agent = GetComponent<UnityEngine.AI.NavMeshAgent>();
        myTransform = transform;
        raycastLayer = 1 << LayerMask.NameToLayer("Player");
        if (isServer)
        {
            StartCoroutine(DoCheck());
        }
        myAnim = GetComponent<Animator>();
        myAnim.SetTrigger("run");
    }
	

    void SearchForTarget() //for finding targets
    {

        if (!isServer) //if not on server
        {
            return; 
        }

        //When on Server
        if(targetTransform == null) //if not set
        {
            Collider[] hitColliders = Physics.OverlapSphere(myTransform.position, radius, raycastLayer);

            if(hitColliders.Length > 0)
            {
                int randomint = Random.Range(0, hitColliders.Length); //random target selection
                targetTransform = hitColliders[randomint].transform;
            }
        }

        if(targetTransform!=null && targetTransform.GetComponent<CapsuleCollider>().enabled == false) //once player is dead set to null and choose the next to attack
        {
            targetTransform = null;
        }
    }

    void MoveToTarget() //move to the target
    {

        if(targetTransform !=null && isServer)
        {
            SetNavDestination(targetTransform);
        }
    }

    void SetNavDestination(Transform dest)
    {
        agent.SetDestination(dest.position);
    }

    IEnumerator DoCheck() //sake of effiency, to avoid doing it in fixedupdate
    {
        for (;;) //endless loop
        {
            SearchForTarget();
            MoveToTarget();
            yield return new WaitForSeconds(0.2f); //runs every .2 seconds
        }
    }
}
