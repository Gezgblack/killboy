﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

[Serializable]
public class ActorContainer
{

    // To store our different actors data
    public List<ActorData> actors = new List<ActorData>();
}

