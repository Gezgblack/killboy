﻿using UnityEngine;
using System.Collections;
using System;

public class highScores : MonoBehaviour { 

    const string privateCode = "7LhGlYaKfEyMGFr38q3McQwt9nOlXKnUqT2newolYdsA";
    const string publicCode = "58a480af68fc111e8078936b";
    const string webURL = "http://dreamlo.com/lb/";

    public Highscore[] highscoresList; //to keep all the high scores
    displayHighScores highscoresDisplay;

    void Awake()
    {
        highscoresDisplay = GetComponent<displayHighScores>();
    }

    public void AddNewHighScore(string username, float score) // to start the couritne, helper method
    {
        StartCoroutine(UploadNewHighscore(username, score));
    }

    public void DownloadScores()
    {
        StartCoroutine("DownloadHighScores");
        /* Dreamlo score Format
        {name}|{score}|x||{date} {time}
        */
    }

    #region couritines

    // To Upload our score
    IEnumerator UploadNewHighscore(string username, float score)
    {
        WWW www = new WWW(webURL + privateCode + "/add/" + WWW.EscapeURL(username) + "/" + score); // upload to the following URL with the following usernmae and score, private because we don't want anyone to just upload
        yield return www; //wait until uploaded

        // Checking our www status
        if (string.IsNullOrEmpty(www.error))
        {
            Debug.Log("Upload Successful");
        }
        else
        {
            Debug.Log("Error uploading " + www.error);
        }
    }

    // To Download our Scores from database
    IEnumerator DownloadHighScores()
    {
        WWW www = new WWW(webURL + publicCode + "/pipe/"); // download the scores from the publicCode
        yield return www; //wait until downloaded

        // Checking our www status
        if (string.IsNullOrEmpty(www.error))
        {
            FormatHighScores(www.text);
            highscoresDisplay.OnHighscoresDownloaded(highscoresList);
        }
        else
        {
            Debug.Log("Error downloading " + www.error);
        }
    }

    #endregion

    void FormatHighScores(string textStream) // To Format our scores to our specifications
    {
        string[] enteries = textStream.Split(new char[] { '\n' }, System.StringSplitOptions.RemoveEmptyEntries); // Add an entry for each componenet of the pipe, seperating the different data from the original dreamlo format, don't count the empty lines
        highscoresList = new Highscore[enteries.Length];

        for(int i=0; i<enteries.Length; i++) // extracting username and score from the formated download
        {
            string[] entryInfo = enteries[i].Split(new char[] { '|' }); // Split into the different fields, the pipe | signifies a split as shown in the format above with accordance with Dreamlo
            string username = entryInfo[0]; // username is split here
            float score = float.Parse(entryInfo[1]); // score is split here
            highscoresList[i] = new Highscore(username, score); // create the new score with the newly split datas
            print(highscoresList[i].username + ": " + highscoresList[i].score);
        }
        //Array.Reverse(highscoresList);
    }
}

public struct Highscore
{
    public string username;
    public float score;

    public Highscore(string usernameIn, float scoreIn)
    {
        username = usernameIn;
        score = scoreIn;
    }
}
