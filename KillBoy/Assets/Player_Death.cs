﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.UI;

public class Player_Death : NetworkBehaviour {

    private Player_Health healthScript; //actual health
    private Image crossHairImage;

    public override void PreStartClient()
    {
        healthScript = GetComponent<Player_Health>(); //find our player health script
        healthScript.EventDie += DisablePlayer; //subscribing the method to the event
    }


    public override void OnNetworkDestroy() //unsubscribe to avoid memory leak
    {
        healthScript.EventDie -= DisablePlayer; //unsubscribing the method from the event
    }


    void DisablePlayer() //on player death disable the following
    {
        GetComponent<CharacterController>().enabled = false; //player controls      
        GetComponent<Player_Shoot>().enabled = false;
        GetComponent<CapsuleCollider>().enabled = false;

        //Disabling the renderers
        Renderer[] renderers = GetComponentsInChildren<Renderer>(); //to get all of the renderers as there are multiple
        foreach(Renderer ren in renderers)
        {
            ren.enabled = false; //diable all of them
        }

        healthScript.isDead = true;

        if (isLocalPlayer) //only disable crosshair on local player
        {
            GetComponent<UnityStandardAssets.Characters.FirstPerson.FirstPersonController>().enabled = false;
            crossHairImage.enabled = false;
        }
    }
}
