﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.IO;

public class GameController : MonoBehaviour
{ // to tell unity what it has to do, a communicator to be used with unity

    // trigger buttons
    public Button saveButton;
    public Button loadButton;

    private static string dataPath = string.Empty;

    void Awake()
    {
        dataPath = System.IO.Path.Combine(Application.persistentDataPath, "actors.json"); // persistentDataPath unity save game path, actors.json name of the file containing actors 
    }

    // for button
    public void Save()
    {
        SaveData.Save(dataPath, SaveData.actorContainer);
    }

    // for button 
    public void Load()
    {
        SaveData.Load(dataPath);
    }
}