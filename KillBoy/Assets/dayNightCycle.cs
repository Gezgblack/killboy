﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class dayNightCycle : NetworkBehaviour { //changes day and night based on the wavelevel SpawnManager_waveLevel.cs script

    [SerializeField]
    public Light light;

    [SerializeField]
    SpawnManager_waveLevel level;

    [SyncVar(hook = "OnLightAmountChange")]
    public float lightAmout = 0f;

    public override void OnStartLocalPlayer()
    {
        OnLightAmountChange(lightAmout);
    }

    void OnLightAmountChange(float amount)
    {
        light.intensity = amount;
    }
}
