﻿using UnityEngine;
using System.Collections;

public class playerControls : MonoBehaviour {

    #region

    //Vars

    //Movements Vars
    public float runSpeed;
    public float walkSpeed;
    Rigidbody mybody;
    Animator myAnimator;
    bool playerDirectionE;
    bool running; //defining whether player is running or walking

    //Jumping Vars
    bool grounded = false;
    Collider[] groundCollisions;
    float groundCheckRadius = 15.0f;
    public LayerMask groundLayer;
    public Transform groundCheck;
    public float jumpHeight;

    #endregion

    // Use this for initialization
    void Start () {
        mybody = GetComponent<Rigidbody>();
        myAnimator = GetComponent<Animator>();
        playerDirectionE = true;
        mybody.transform.eulerAngles = new Vector3(0, 90, 0);

        
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void FixedUpdate()
    {
        running = false; //defanition 

        if(grounded && Input.GetAxis("Jump") > 0)
        {
            grounded = false;
            myAnimator.SetBool("grounded", grounded);
            mybody.AddForce(new Vector3(0, jumpHeight, 0));
        }

        groundCollisions = Physics.OverlapSphere(groundCheck.position, groundCheckRadius, groundLayer);
        if (groundCollisions.Length > 0) grounded = true;
        else grounded = false;

        myAnimator.SetBool("grounded", grounded);

        float move = Input.GetAxis("Horizontal");
        myAnimator.SetFloat("speed", Mathf.Abs(move)); //To intiate the charecter transition(move)

        // Sneaking Check
        float sneaking = Input.GetAxisRaw("Fire3"); //Fire3 = Shift Button
        myAnimator.SetFloat("sneaking", sneaking);

        float firing = Input.GetAxis("Fire1");
        myAnimator.SetFloat("shooting", firing);

        if ((sneaking > 0 || firing > 0) && grounded) //if we are sneaking and we are on the ground, and if we are firing the gun
        {
            mybody.velocity = new Vector3(move * walkSpeed, mybody.velocity.y, 0); //move charecter along the x axis, and keep y on gravity, not touching the z axis Walk
        }
        else {
            mybody.velocity = new Vector3(move * runSpeed, mybody.velocity.y, 0); //move charecter along the x axis, and keep y on gravity, not touching the z axis Run
            if (Mathf.Abs(move) > 0) { running = true; } //if move value is greater than 0, we are running
        }

        if (move>0 && !playerDirectionE) // Change direction left
        {
            Flip();
        }
        else if(move<0 && playerDirectionE) // Change direction right
        {
            Flip();
        }
    }

    void Flip() //Flip character to change the direction
    {
        playerDirectionE = !playerDirectionE;
        Vector3 theScale = transform.localScale;
        theScale.z *= -1;
        transform.localScale = theScale;
    }

    //check if we are facing east(right)
    public float GetFacing()
    {
        if (playerDirectionE)
        {
            return 1; //yes
        }
        else
        {
            return -1; //no
        }
    }

    public bool getRunning() //public so we can access in other scripts, will tell if player is running or not
    {
        return running; 
    }
}
