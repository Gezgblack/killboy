﻿using UnityEngine;
using System.Collections;

public class garageDoorController : MonoBehaviour {

    public bool resetable; // reset door into states open/close
    public GameObject door;
    public GameObject gear;
    public bool startOpen; //starting opened or close

    bool firstTrigger = false; //opening trigger
    bool open = true; //opening var

    Animator doorAnim;
    Animator gearAnim;

    AudioSource doorAudio;

	// Use this for initialization
	void Start () {

        doorAnim = door.GetComponent<Animator>(); //door animator
        gearAnim = gear.GetComponent<Animator>(); //gear animator
        doorAudio = door.GetComponent<AudioSource>(); //door sounds

        if (!startOpen) //if door is not opened, the set to close in the start
        {
            open = false;
            doorAnim.SetTrigger("doorTrigger");
            doorAudio.Play();
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player" && !firstTrigger)
        {
            if (!resetable) firstTrigger = true;
                doorAnim.SetTrigger("doorTrigger"); //move door
                open = !open; //no longer open
                gearAnim.SetTrigger("gearRotating");
                doorAudio.Play();
            
        }
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
