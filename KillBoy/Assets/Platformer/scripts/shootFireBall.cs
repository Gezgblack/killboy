﻿using UnityEngine;
using System.Collections;

public class shootFireBall : MonoBehaviour { //responsible for creating fire ball damage

    public float damage; //amount of damage
    public float speed; //travel speed of the fire ball

    Rigidbody myRB;

	// Use this for initialization
	void Start () {

        myRB = GetComponentInParent<Rigidbody>(); //in the parent of the location

        if(transform.rotation.y > 0) //if we are facing one of the directions
        {
            myRB.AddForce(Vector3.right * speed, ForceMode.Impulse); //move the fireball right along the x axis
        }
        else
        {
            myRB.AddForce(Vector3.right * -speed, ForceMode.Impulse); //move the fireball left along the x axis -speed
        }
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Enemy" || other.gameObject.layer == LayerMask.NameToLayer("Shootable")) //if we hit something which is of the layer shootable do this
        {
            myRB.velocity = Vector3.zero; //stop the velocity of the object(fire ball)
            enemyHealth theEnemyHealth = other.GetComponent<enemyHealth>(); //get enemy health
            if(theEnemyHealth != null) //if we find it and its not null
            {
                theEnemyHealth.addDamage(damage); //add the damage
                theEnemyHealth.addFire(); //turn terget on fire
            }
            Destroy(gameObject); //Destroy the fire ball
        }
    }
}
