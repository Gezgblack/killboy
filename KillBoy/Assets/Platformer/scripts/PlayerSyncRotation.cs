﻿/*using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System.Collections.Generic;

public class PlayerSyncRotation : NetworkBehaviour {

    [SyncVar(hook ="OnPlayerRotSynced")]
    private float syncPlayerRotation;

    [SerializeField]
    private Transform playerTransform;
    private float lerpRate = 15;

    private float lastPlayerRot;
    private float threshold = 1;

    private List<float> syncPlayerRotList = new List<float>();
    private float closeEneough = 0.3f;
    [SerializeField]
    private bool userHistoricalInterpolation;

    [Client]
    void OnPlayerRotSynced(float latestPlayerRotation)
    {
        syncPlayerRotation = latestPlayerRotation;
        syncPlayerRotList.Add(syncPlayerRotation);
    }

    [Command]
    void CmdProvideRotationsToServer(float playerRot)
    {
        syncPlayerRotation = playerRot;
    }

    [Client]
    void transmitRotations()
    {
        if (isLocalPlayer)
        {
            if(CheckIfBeyondThreshold(playerTransform.localScale.z, lastPlayerRot)){
                lastPlayerRot = playerTransform.localScale.z;
                CmdProvideRotationsToServer(lastPlayerRot);
                    
            }
        }
    }

    bool CheckIfBeyondThreshold(float rot1, float rot2)
    {
        if (Mathf.Abs(rot1 - rot2) > threshold)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        lerpRotation();
	}

    void FixedUpdate()
    {
        transmitRotations();
    }

    void lerpRotation()
    {
        if (!isLocalPlayer)
        {
                HistoricalInterpolation();
        }
    }

    void HistoricalInterpolation(){
        if (syncPlayerRotList.Count > 0)
        {
            LerpPlayerRotation(syncPlayerRotList[0]);

            if(Mathf.Abs(playerTransform.localEulerAngles.z - syncPlayerRotList[0]) < closeEneough)
            {
                syncPlayerRotList.RemoveAt(0);
            }
            Debug.Log(syncPlayerRotList.Count.ToString() + "syncPlayerRotList Count");
        }
    }

    void LerpPlayerRotation(float rotAngle)
    {
        Vector3 playerNewRot = new Vector3(0, 0, rotAngle);
        playerTransform.rotation = Quaternion.Lerp(playerTransform.rotation, Quaternion.Euler(playerNewRot),lerpRate*Time.deltaTime);
    }
}
*/