﻿using UnityEngine;
using System.Collections;

public class randomiseZombieAppearance : MonoBehaviour {

    public Material[] zombieMaterials; //to holde the different textures

	// Use this for initialization
	void Start () {

        SkinnedMeshRenderer myRenderer = GetComponent<SkinnedMeshRenderer>(); //find the renderer
        myRenderer.material = zombieMaterials[Random.Range(0, zombieMaterials.Length)]; //pick one of the textures randomly
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
