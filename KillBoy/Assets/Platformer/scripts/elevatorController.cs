﻿using UnityEngine;
using System.Collections;

public class elevatorController : MonoBehaviour {

    public float resetTime; //stop time for the elevator 

    Animator elevAnim; //animation
    AudioSource elevAS; //sound

    float downTime;
    bool elevIsUp = false;

	// Use this for initialization
	void Start () {

        elevAnim = GetComponent<Animator>(); //find animator
        elevAS = GetComponent<AudioSource>(); //find sound source
	}
	
	// Update is called once per frame
	void Update () {

        if (downTime <= Time.time && elevIsUp)//if elevator is up and we have reached our time 
        {
            elevAnim.SetTrigger("activateElevator"); //trigger elevator
            elevIsUp = false;
            elevAS.Play();
        }
	}

    void OnTriggerEnter(Collider other) //when triggered
    {
        if(other.tag == "Player" && !elevIsUp) //if the player gameobject is on the collider do
        {
            elevAnim.SetTrigger("activateElevator"); //set motion
            downTime = Time.time + resetTime; //elevator goes back to position after a particular amount of time
            elevIsUp = true;
            elevAS.Play(); //play sound
        }
    }
}
