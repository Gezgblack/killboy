﻿using UnityEngine;
using System.Collections;

public class healthPickupController : MonoBehaviour {

    public float healthAmount; //the amount of health given on pickup
    public AudioClip healthPickupSound; //sound on pickup

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player" && other.GetComponent<playerHealth>().currentHealth < other.GetComponent<playerHealth>().fullHealth) //if the player is colliding and the players health is not full
        {
            other.GetComponent<playerHealth>().addHealth(healthAmount); //access playerHealth and add health
            Destroy(transform.root.gameObject); //destroy the heart
            AudioSource.PlayClipAtPoint(healthPickupSound, transform.position, 5f); //play sound at this point
        }
    }
}
