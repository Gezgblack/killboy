﻿using UnityEngine;
using System.Collections;

public class ammoPickupController : MonoBehaviour {

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player" && other.GetComponentInChildren<fireBullet>().remainingRounds < other.GetComponentInChildren<fireBullet>().maxRounds) //if he player layer is colliding with the collider and the ammo is not full
        {
            other.GetComponentInChildren<fireBullet>().reload(); //find fireBullet script and use the reload method
            Destroy(transform.root.gameObject); //destroy ammo pickup
        }
    }
}
