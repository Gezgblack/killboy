﻿using UnityEngine;
using System.Collections;

public class meleeScript : MonoBehaviour { //Responsible for everything that happens in Melee, this class will be interchaneable with other weapons that are melee eg swords

    public float damage; //how much damage the weapon does
    public float knockBack; //how far the weapon will push enemies back
    public float knockBackRadius; //the radius of the knockback
    public float meleeRate; //how often you can melee attack eg heavy, light weapons

    float nextMelee; //the next time you can swing
    int shootableMask; //determine what can be hit

    Animator myAnim;
    playerControls myPC;

	// Use this for initialization
	void Start () {

        shootableMask = LayerMask.GetMask("Shootable"); //select what we can actually hit
        myAnim = transform.root.GetComponent<Animator>(); //reference to the animator on our soldier
        myPC = transform.root.GetComponent<playerControls>(); //link to our playerConroller script
        nextMelee = 0f; //no delay on the swing, it is instant
	
	}
	
	// Update is called once per frame
	void FixedUpdate () {

        float melee = Input.GetAxis("Fire2"); //link to our action control, right mouse button

        if (melee > 0 && nextMelee < Time.time && !(myPC.getRunning())) { //if we are pressing the button and we are not running 
            myAnim.SetTrigger("gunMelee"); //trigger the animation 
            nextMelee = Time.time + meleeRate; //the next time we can swing

            //do damage
            Collider[] attacked = Physics.OverlapSphere(transform.position, knockBackRadius, shootableMask); //anything at the tip of the gun will be hit by our sphere, how big the sphere is, and the mask shootable, returns anything hit by our sphere

            int i = 0;
            while (i < attacked.Length) // loop through the attacked colliders
            {
                if(attacked[i].tag == "Enemy") //if the attacked entity is actually enemy
                {
                    enemyHealth doDamage = attacked[i].GetComponent<enemyHealth>(); //find enemy script
                    doDamage.addDamage(damage); // add appropriate amount of damage
                    doDamage.damageFX(transform.position, transform.localEulerAngles); //damage effects
                }
                i++; //go through while
            }
        }
	}
}
