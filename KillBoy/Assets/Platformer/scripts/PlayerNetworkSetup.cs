﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class PlayerNetworkSetup : NetworkBehaviour {

    public override void OnStartLocalPlayer()
    {
        GetComponent<playerControls>().enabled = true;

        GetComponent<NetworkAnimator>().SetParameterAutoSend(0, true); //Sending the animation Host     
    }

    public override void PreStartClient()
    {
        GetComponent<NetworkAnimator>().SetParameterAutoSend(0, true); //Sending the animation Client
    }

}
