﻿using UnityEngine;
using System.Collections;

public class cleaner : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter(Collider other) //Destroy whatever falls into the collider
    {
        if (other.tag == "Player") //If player falls
        {
            playerHealth playerDead = other.gameObject.GetComponent<playerHealth>(); //playerHealth script command access granted here
            playerDead.makeDead(); //player Destroy method from playerHealth
        }
        else //if anything that's not a player falls into the collider Destroy it too
        {
            Destroy(gameObject);
        }
    }
}
