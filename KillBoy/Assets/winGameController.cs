﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class winGameController : MonoBehaviour {

    //HUD
    public Text endGameText;
    scoreManager scoreManager;


    // Use this for initialization
    void Start () {

        scoreManager = FindObjectOfType<scoreManager>();
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            scoreManager.scoreIncreasing = false; //stop score from increasing any further
            int finalScore = (int)Mathf.Ceil(scoreManager.scoreCount);
            PlayerPrefs.SetInt("playerScore", finalScore);
            endGameText.text = "Game Complete";
            Animator endGameAnim = endGameText.GetComponent<Animator>();
            endGameAnim.SetTrigger("endGame");
            Debug.Log("SCORE!!!");
            Debug.Log(PlayerPrefs.GetInt("playerScore"));
            Destroy(other.gameObject);
        }
    }
}
