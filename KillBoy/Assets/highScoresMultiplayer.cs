﻿using UnityEngine;
using System.Collections;

public class highScoresMultiplayer : MonoBehaviour {

    const string privateCode = "8gCv5hTz7kiG1YNqta69xgSEZBidZEq0SrvYF7eFUsYw";
    const string publicCode = "58e0515212e7f10688b9365c";
    const string webURL = "http://dreamlo.com/lb/";

    public highScoreMultiplayer[] highscoresList; //to keep all the high scores
    displayHighScoresMultiplayer highscoresDisplay;

    void Awake()
    {
        highscoresDisplay = GetComponent<displayHighScoresMultiplayer>();
    }

    public void AddNewHighScore(string gameName, float score) // to start the couritne, helper method
    {
        StartCoroutine(UploadNewHighscore(gameName, score));
    }

    public void DownloadScores()
    {
        StartCoroutine("DownloadHighScores");
        /* Dreamlo score Format
        {name}|{score}|x||{date} {time}
        */
    }

    // To Upload our score
    IEnumerator UploadNewHighscore(string gameName, float score)
    {
        WWW www = new WWW(webURL + privateCode + "/add/" + WWW.EscapeURL(gameName) + "/" + score); // upload to the following URL with the following usernmae and score, private because we don't want anyone to just upload
        yield return www; //wait until uploaded

        // Checking our www status
        if (string.IsNullOrEmpty(www.error))
        {
            Debug.Log("Upload Successful");
        }
        else
        {
            Debug.Log("Error uploading " + www.error);
        }
    }

    // To Download our Scores from database
    IEnumerator DownloadHighScores()
    {
        WWW www = new WWW(webURL + publicCode + "/pipe/"); // download the scores from the publicCode
        yield return www; //wait until downloaded

        // Checking our www status
        if (string.IsNullOrEmpty(www.error))
        {
            FormatHighScores(www.text);
            highscoresDisplay.OnHighscoresDownloaded(highscoresList);
        }
        else
        {
            Debug.Log("Error downloading " + www.error);
        }
    }

    void FormatHighScores(string textStream) // To Format our scores to our specifications
    {
        string[] enteries = textStream.Split(new char[] { '\n' }, System.StringSplitOptions.RemoveEmptyEntries); // Add an entry for each componenet of the pipe, seperating the different data from the original dreamlo format, don't count the empty lines
        highscoresList = new highScoreMultiplayer[enteries.Length];

        for (int i = 0; i < enteries.Length; i++) // extracting username and score from the formated download
        {
            string[] entryInfo = enteries[i].Split(new char[] { '|' }); // Split into the different fields, the pipe | signifies a split as shown in the format above with accordance with Dreamlo
            string gameName = entryInfo[0]; // username is split here
            float score = float.Parse(entryInfo[1]); // score is split here
            highscoresList[i] = new highScoreMultiplayer(gameName, score); // create the new score with the newly split datas
            print(highscoresList[i].gameName + ": " + highscoresList[i].score);
        }
    }
}

public struct highScoreMultiplayer
{
    public string gameName;
    public float score;

    public highScoreMultiplayer(string gameNameIn, float scoreIn)
    {
        gameName = gameNameIn;
        score = scoreIn;
    }
}
