﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

public class Chat : NetworkBehaviour {

    Text TxtTexto;
    InputField inputField;


    public GameObject messagePrefab;

    // Use this for initialization
    void Start () {

        TxtTexto = GameObject.Find("textMessage").GetComponent<Text>();
        inputField = GameObject.Find("MessageInput").GetComponent<InputField>();
	}
	
	// Update is called once per frame
	void Update () {

        if (!isLocalPlayer)
            return;

        //only on local player
        if (Input.GetKeyDown(KeyCode.Return))
        {
            if(inputField.text != "")
            {
                string Mensaje = inputField.text;
                inputField.text = "";

                CmdEnviar(Mensaje);
            }
        }

        if (Input.GetKeyDown(KeyCode.Y))
        {
            inputField.ActivateInputField();
            Cursor.visible = true;
        }
    }

    [Command]
    void CmdEnviar(string mensaje)
    {
        RpcRecivir(mensaje);
    }

    [ClientRpc]
    public void RpcRecivir(string mensaje)
    {
        TxtTexto.text += ">>" + mensaje + "\n";
    }
}
