﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class Player_SyncPosition : NetworkBehaviour {

    [SyncVar] //automatically transmit to client on change by the server
    private Vector3 syncPos;

    [SerializeField]
    Transform myTransform; //out player transoform ref
    [SerializeField]
    float lerpRate = 15;

    private Vector3 lastPos;
    private float threshold = 0.5f; //send half a meter changes, not exact down to the milimeter changes
	
	// Update is called once per frame
	void FixedUpdate () {

        TransmitPosition(); //send to server
        LerpPosition(); //lerp position
	}

    void LerpPosition() //only happening on players that are not our own, so whoever you are playing with
    {
        if (!isLocalPlayer) //lerp on outside player, not current player
        {
            myTransform.position = Vector3.Lerp(myTransform.position, syncPos, Time.deltaTime * lerpRate); //lerp the position of the player at the rate we specify
        }
    }

    [Command] //sent to server, from client to server
    void CmdProvidePostionToServer(Vector3 pos) //to run in the server and called by the client, by our charecter
    {
        syncPos = pos;
    }

    [ClientCallback] //for clients
    void TransmitPosition() //to call the CmdProvidePostionToServer method to send position to server and inturn server sends to clients via [SyncVar]
    {
        if (isLocalPlayer && Vector3.Distance(myTransform.position, lastPos) > threshold) //on client, if local and the distance changed is greater then threshold then run command
        {
            CmdProvidePostionToServer(myTransform.position);
            lastPos = myTransform.position; //to keep track of last postion
        }
    }
}
