﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class Player_ID : NetworkBehaviour {

    [SyncVar]
    public string playerUniqueIdentity; //to use the id as a string
    private NetworkInstanceId playerNetId; //to get the id
    private Transform myTransform;

    public NetworkIdentity netId;

    public override void OnStartLocalPlayer()
    {
        GetNetIdentity();
        SetIdentity();
    }

    // Use this for initialization
    void Awake () {

        myTransform = transform;

	}
	
	// Update is called once per frame
	void Update () {
	
        if(myTransform.name == "" || myTransform.name == "Player(Clone)") //if the name is empty or is the default unity prefab clone, do
        {
            SetIdentity(); //set identity
        }

	}

    [Client] //only ran on client side
    void GetNetIdentity()
    {
        playerNetId = GetComponent<NetworkIdentity>().netId; //get the player id from the NetworkIdentity component
        CmdTellServerMyIdentity(MakeUniqueIdentity()); //send the name to the server
        netId = GetComponent<NetworkIdentity>();
    }

    void SetIdentity()
    {
        if (!isLocalPlayer) //if not local player
        {
            myTransform.name = playerUniqueIdentity;
        }
        else
        {
            myTransform.name = MakeUniqueIdentity();
        }
    }

    string MakeUniqueIdentity() //naming scheme
    {
        string uniqueName = "Player " + playerNetId.ToString();
        return uniqueName;
    }

    [Command]
    void CmdTellServerMyIdentity(string name)
    {
        playerUniqueIdentity = name;
    }

    public void DisconnectPlayer()
    {
        netId.connectionToClient.Disconnect();
    }
}
