﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.UI;

public class Player_Shoot : NetworkBehaviour
{

    private int damage = 40;
    private float range = 1500;
    [SerializeField]
    private Transform camTransform;
    private RaycastHit hit;

    Ray shootRay;
    RaycastHit shootHit; //register hits
    int shootableMask; //determine what can be shot
    LineRenderer gunLine; //to change to bullet

    public int playerMoney = 0; //The players money count

    Text playerMoneyText;

    public override void OnStartLocalPlayer()
    {
        playerMoneyText = GameObject.Find("moneyCount").GetComponent<Text>(); //find our health slider
        SetMoneyText();
    }

    // Update is called once per frame
    void Update()
    {
        CheckIfShooting();
    }

    void CheckIfShooting()
    {
        if (!isLocalPlayer)
        {
            return;
        }

        if (Input.GetAxisRaw("Fire1") > 0)
        {
            Shoot();
        }
        SetMoneyText();
    }

    [Client]
    void Shoot()
    {
        shootableMask = LayerMask.GetMask("Shootable"); //Shootable layers, in layers. Find the shootable layer.
        gunLine = GetComponent<LineRenderer>(); //line renderer reference

        shootRay.origin = transform.position; //Ray is being shot from where instantiated 
        shootRay.direction = transform.forward; //Shoot forward from position
        //gunLine.SetPosition(0, transform.position); //where to start and finish drawing

        if (Physics.Raycast(shootRay, out shootHit, range, shootableMask)) //chose the ray, out value will bring information as to what we hit, range is how far it will shoot, layermask to determine what can be hit/shot (shootable)
        {
            if (shootHit.collider.tag == "Zombie") //if we have shot an enemy
            {
                playerMoney++; //increase the players money by 1 each time zombie is shot
                SetMoneyText();
                string uIdentity = shootHit.transform.name;
                CmdTellServerWhichZombieWasShot(uIdentity, damage);

            }
            //Hit an enemy will be entered here
            //gunLine.SetPosition(1, shootHit.point); //endpoint of gunline, what did i hit, where did i hit it
        }

    }

    void SetMoneyText() //to set money locally
    {
        if (isLocalPlayer) //only done by the local player
        {
            playerMoneyText.text = playerMoney.ToString();
            if(playerMoney <= 0)
            {
                playerMoney = 0;
            }
        }
    }


    [Command]
    void CmdTellServerWhichZombieWasShot(string uniqueID, int damage)
    {
        GameObject go = GameObject.Find(uniqueID);
        go.GetComponent<Zombie_Health>().DeductHealth(damage); //call method to deduct amount of health specified
    }
}
