﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class MainMenuActions : MonoBehaviour {

    public GameObject panelMainMenu = new GameObject("Panel");
    public GameObject panelSingleplayer = new GameObject("Panel");
    public GameObject panelDifficulty = new GameObject("Panel");
    public GameObject panelScoreBoard = new GameObject("Panel");
    public GameObject panelMultiplayer = new GameObject("Panel");
    public GameObject panelScoreBoardMultiplayer = new GameObject("Panel");
    public GameObject controlsPanel = new GameObject("Panel");
    public string difficultyChooser = ""; //for choosing difficulty

    public Button singlePlayerButton;

    public void singlePlayerPressedFromMainMenu()
    {
        panelMainMenu.SetActive(false);
        panelSingleplayer.SetActive(true);
    }

    public void backPressedFromSinglePlayer()
    {
        panelSingleplayer.SetActive(false);
        panelMainMenu.SetActive(true);
        panelDifficulty.SetActive(false);
        panelScoreBoard.SetActive(false);
    }

    public void playPressedFromSinglePlayer()
    {
        panelDifficulty.SetActive(true);
        panelSingleplayer.SetActive(false);
        //SceneManager.LoadScene(1);
    }

    public void setToDifficultyToEasy() //for unit testing purpose
    {
        difficultyChooser = "Easy";
    }

    public void setToDifficultyToNormal() //for unit testing purpose
    {
        difficultyChooser = "Normal";
    }

    public void easyPressedFromSinglePlyaer()
    {
        setToDifficultyToEasy();
        PlayerPrefs.SetString("gameDifficulty", difficultyChooser);
        SceneManager.LoadScene(1);
    }

    public void normalPressedFromSinglePlyaer()
    {
        setToDifficultyToNormal();
        PlayerPrefs.SetString("gameDifficulty", difficultyChooser);
        SceneManager.LoadScene(1);
    }

    public void hardPressedFromSinglePlyaer()
    {
        difficultyChooser = "Hard";
        PlayerPrefs.SetString("gameDifficulty", difficultyChooser);
        SceneManager.LoadScene(1);
    }

    public void scoreBoardPressedFromMainMenu()
    {
        panelMainMenu.SetActive(false);
        panelScoreBoard.SetActive(true);
    }

    public void multiplayerPressedFrom()
    {
        panelMainMenu.SetActive(false);
        panelMultiplayer.SetActive(true);
    }

    public void playPressedInMultiplayer()
    {
        SceneManager.LoadScene(4);
    }

    public void scoreBoardPressedFromMultiplayer()
    {
        panelMultiplayer.SetActive(false);
        panelScoreBoardMultiplayer.SetActive(true);
    }

    public void backPressedFromHighscoresMultiplayers()
    {
        panelScoreBoardMultiplayer.SetActive(false);
        panelMultiplayer.SetActive(true);
    }

    public void backPressedFromMultiplayerPanelToMain()
    {
        panelMultiplayer.SetActive(false);
        panelMainMenu.SetActive(true);
    }

    public void openControls()
    {
        controlsPanel.SetActive(true);
        panelMainMenu.SetActive(false);
    }

    public void backFromControls()
    {
        panelMainMenu.SetActive(true);
        controlsPanel.SetActive(false);
    }

    public void exitPressedFromMainMenu()
    {
        Application.Quit();
    }
}
