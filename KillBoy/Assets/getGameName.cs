﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class getGameName : MonoBehaviour {

    [SerializeField]
    InputField gameName;
	
    void Start()
    {
        PlayerPrefs.DeleteKey("gameName");
    }

    public void buttonPressedToSaveGameName()
    {
        string name = gameName.text;
        PlayerPrefs.SetString("gameName", name);
    }
}
