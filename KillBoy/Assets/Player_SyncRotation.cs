﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System.Collections.Generic;

public class Player_SyncRotation : NetworkBehaviour {

    [SyncVar(hook ="OnPlayerRotSync")]
    private float syncPlayerRotation;
    [SyncVar(hook = "OnCamRotSync")]
    private float syncCamRotation;

    [SerializeField]
    private Transform playerTransform;
    [SerializeField]
    private Transform camTrasform;
    
    private float lerpRate = 20;

    private float lastPlayerRot;
    private float lastCamRot;
    private float threshold = 1; //degree threshold

    private List<float> syncPlayerRotList = new List<float>();
    private List<float> syncCamRotList = new List<float>();
    private float closeEnough = 0.4f;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void FixedUpdate () {

        TransmitRotation();
        LerpRotation();
	}

    void LerpRotation()
    {
        if (!isLocalPlayer)
        {
            OrdinaryLerping();
        }

            //playerTransform.rotation = Quaternion.Lerp(playerTransform.rotation, syncPlayerRotation, Time.deltaTime * lerpRate); //the player model rotation
            //camTrasform.rotation = Quaternion.Lerp(camTrasform.rotation, syncCamRotation, Time.deltaTime * lerpRate); //the camera rotation
        }
    

    void OrdinaryLerping()
    {
        LerpPlayerRotation(syncPlayerRotation);
        LerpCamRot(syncCamRotation);
    }

    void LerpPlayerRotation(float rotAngle)
    {
        Vector3 playerNewRot = new Vector3(0, rotAngle, 0);
        playerTransform.rotation = Quaternion.Lerp(playerTransform.rotation, Quaternion.Euler(playerNewRot), lerpRate * Time.deltaTime);
    }

    void LerpCamRot(float rotAngle)
    {
        Vector3 camNewRot = new Vector3(rotAngle, 0, 0);
        camTrasform.localRotation = Quaternion.Lerp(camTrasform.localRotation, Quaternion.Euler(camNewRot), lerpRate * Time.deltaTime);
    }

    [Command] //to send the rotation information to the server
    void CmdProvideRotationToServer(float playerRot, float camRot) //send both player and camera rotations
    {
        syncPlayerRotation = playerRot;
        syncCamRotation = camRot;
    }

    [Client] //for client
    void TransmitRotation() //to call the CmdProvideRotationToServer method to send position to server and inturn server sends to clients via [SyncVar], both player and camera
    {
        if (isLocalPlayer) //on client, 
        {
            //if(Quaternion.Angle(playerTransform.rotation, lastPlayerRot) > threshold || Quaternion.Angle(camTrasform.rotation, lastCamRot) > threshold)
            if(CheckIfBeyondThreshold(playerTransform.localEulerAngles.y, lastPlayerRot) || CheckIfBeyondThreshold(camTrasform.localEulerAngles.x, lastCamRot)) //if the rotation changed is greater then the threshold and the cam rotation is greater then threshold send rotations
            {
                lastPlayerRot = playerTransform.localEulerAngles.y;
                lastCamRot = camTrasform.localEulerAngles.x;
                CmdProvideRotationToServer(lastPlayerRot, lastCamRot);
            }
        }
    }

    bool CheckIfBeyondThreshold(float rot1, float rot2)
    {
        if(Mathf.Abs(rot1-rot2) > threshold)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    [Client]
    void OnPlayerRotSync(float latestPlayerRot)
    {
        syncPlayerRotation = latestPlayerRot;
        syncPlayerRotList.Add(syncPlayerRotation);
    }

    [Client]
    void OnCamRotSync(float latestCamRot)
    {
        syncCamRotation = latestCamRot;
        syncCamRotList.Add(syncCamRotation);
    }
}
