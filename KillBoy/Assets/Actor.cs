﻿using UnityEngine;
using System.Collections;
using System;

public class Actor : MonoBehaviour //contains all aspects of the game which we want to save
{

    public ActorData data = new ActorData(); // to store our data

    [SerializeField]
    public playerHealth myHealth; //our health

    [SerializeField]
    public fireBullet myBullets; //our ammo

    [SerializeField]
    public scoreManager myScore;

    public void StoreData() // for storing our data
    {
        //location
        data.pos = transform.position;

        //health
        data.health = myHealth.currentHealth;

        //bullet
        data.bullets = myBullets.remainingRounds;

        //score
        data.score = myScore.scoreCount;
    }

    public void LoadData() // for loading our data
    {
        //location
        var currentActor = SaveData.actorContainer.actors[0];
        transform.position = currentActor.pos;
        data = currentActor;

        //health
        myHealth.currentHealth = data.health;
        myHealth.playerHealthSlider.value = myHealth.currentHealth;

        //bullets
        myBullets.remainingRounds = data.bullets;
        myBullets.playerAmmoSlider.value = myBullets.remainingRounds;

        //score
        myScore.scoreCount = data.score;
        myScore.scoreText.text = "" + Mathf.Round(myScore.scoreCount);
    }

    public void ApplyData()
    {
        SaveData.AddActorData(data);
    }

    void OnEnable() // to enable the save/load to avoid hanging
    {
        SaveData.OnLoaded += LoadData;
        SaveData.OnBeforeSave += StoreData;
        SaveData.OnBeforeSave += ApplyData;
    }

    void OnDisable() // to disable the save/load to avoid hanging
    {
        SaveData.OnLoaded -= LoadData;
        SaveData.OnBeforeSave -= StoreData;
        SaveData.OnBeforeSave -= ApplyData;
    }
}

[Serializable] // for storing in JSON
public class ActorData // our players attributes in JSON
{
    public Vector3 pos;

    public float health;

    public int bullets;

    public float score;
}