﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class pauseMenuMultiplayer : MonoBehaviour
{ //Game Pause Controls

    public GameObject PauseUI; //pause panel

    private bool pause = false;

    [SerializeField]
    Player_ID playerID;

    // Use this for initialization
    void Start()
    {

        PauseUI.SetActive(false); //hide on start
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetButtonDown("Pause")) //escape button
        {
            pause = !pause;
        }

        if (pause)
        {
            PauseUI.SetActive(true); //show
            Time.timeScale = 0;
        }

        if (!pause)
        {
            PauseUI.SetActive(false); //hide
            Time.timeScale = 1;
        }
    }

    public void Quit()
    {
        Application.Quit();
        playerID.DisconnectPlayer();
    }
}
