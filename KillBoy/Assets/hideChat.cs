﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class hideChat : MonoBehaviour {

    [SerializeField]
    public Canvas chat;

    private bool pause = false;

    // Update is called once per frame
    void Update () {

        if (Input.GetKeyDown(KeyCode.C)) //escape button
        {
            pause = !pause;
        }

        if (pause)
        {
            chat.enabled = true;
        }

        if (!pause)
        {
            chat.enabled = false; //hide
        }
    }
}
