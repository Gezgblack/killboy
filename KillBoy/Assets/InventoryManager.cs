﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class InventoryManager : MonoBehaviour { //to switch through all our weapons, manager to control the inventory bihaviour

    public GameObject[] weapons; //our weapons
    bool[] weaponAvailable; //for later weapon pickup idea
    public Image weaponImage; //image of the weapon on the gui

    int currentWeapon;

	// Use this for initialization
	void Start () {

        weaponAvailable = new bool[weapons.Length]; //same size
        for (int i = 0; i < weapons.Length; i++) weaponAvailable[i] = false; //set weapons to false
        currentWeapon = 0;
        //weaponAvailable[currentWeapon] = true; //turn on single weapon
        for (int i = 0; i < weapons.Length; i++) weaponAvailable[i] = true; //set weapons to true, this is for a later weapon pickup idea

        deactivateWeapons();

        setWeaponActive(currentWeapon); //current weapon set
    }
	
	// Update is called once per frame
	void Update () {

        //toggle weapon button
        if (Input.GetButtonDown("Change")) //if enter is hit do
        {
            int i;
            for(i=currentWeapon +1; i <weapons.Length; i++)
            {
                if(weaponAvailable[i] == true)
                {
                    currentWeapon = i;
                    setWeaponActive(currentWeapon); //weapon found
                    return;
                }
            }
            for(i = 0; i<currentWeapon; i++)
            {
                if (weaponAvailable[i] == true)
                {
                    currentWeapon = i;
                    setWeaponActive(currentWeapon); //weapon found
                    return;
                }
            }
        }
	}

    public void setWeaponActive(int whichWeapon)
    {
        if (!weaponAvailable[whichWeapon]) return; //if weapon not available 
        deactivateWeapons();
        weapons[whichWeapon].SetActive(true); //activate chosen weapon
        weapons[whichWeapon].GetComponentInChildren<fireBullet>().initialializeWeapon(); //initialise weapon
    }

    void deactivateWeapons()
    {
        for (int i = 0; i < weapons.Length; i++) weapons[i].SetActive(false); //setting the weapons visability to false
    }

    public void activateWeapon(int whichWeapon)
    {
        weaponAvailable[whichWeapon] = true; //to activate a single weapon
    }
}
