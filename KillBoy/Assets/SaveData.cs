﻿using UnityEngine;
using System.Collections;
using System.IO;

public class SaveData
{

    // every actos will be sorted in this actor container when loading the game
    public static ActorContainer actorContainer = new ActorContainer(); // static so that we dont have to recreacte savedata everytime

    public delegate void SerializeAction();
    public static event SerializeAction OnLoaded; // happens after loading
    public static event SerializeAction OnBeforeSave; // right before saving

    public static void Load(string path) // where the load is
    {
        actorContainer = LoadActors(path);

        OnLoaded();

        ClearActorList(); // so that there's no duplicates
    }

    public static void Save(string path, ActorContainer actors)
    {
        OnBeforeSave(); // to throw of the data that was in OnBeforeSave

        SaveActors(path, actors); // store all of our actors as json in the file

        ClearActorList(); // clear the list so that the next time we save theres no duplicates
    }

    public static void AddActorData(ActorData data)
    {
        actorContainer.actors.Add(data);
    }

    public static void ClearActorList() // to avoid duplicate data
    {
        actorContainer.actors.Clear(); // access actors and clear it
    }

    private static ActorContainer LoadActors(string path) // path to the JSON
    {
        string json = File.ReadAllText(path); // loading all the text out of the file into a string, assuming the text is all JSON

        return JsonUtility.FromJson<ActorContainer>(json); // parse the JSON into an ActorContainer type and return in the LoadActors
    }

    private static void SaveActors(string path, ActorContainer actors) // where to save and the actors to save
    {
        string json = JsonUtility.ToJson(actors);

        StreamWriter sw = File.CreateText(path); // if file doesnt exist, make the file in the specified path
        sw.Close();

        File.WriteAllText(path, json); // fill the file with the data(json)
    }
}