﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class Zombie_MotionSync : NetworkBehaviour { //for better zombie motion syncing over the network

    [SyncVar]
    private Vector3 syncPos;
    [SyncVar]
    private float syncYRot;

    private Vector3 lastPos;
    private Quaternion lastRot;
    private Transform myTransform;
    private float lerpRate = 10;
    private float posThreshold = 0.5f;
    private float rotThreshold = 5;

	// Use this for initialization
	void Start () {

        myTransform = transform;
	}
	
	// Update is called once per frame
	void Update () {

        TrasmitMotion();
        LerpMotion();
	}

    void TrasmitMotion() //to send the motion changes
    {
        //only happening on the server because these are zombies
        if (!isServer)
        {
            return;
        }

        if (Vector3.Distance(myTransform.position, lastPos) > posThreshold || Quaternion.Angle(myTransform.rotation, lastRot)>rotThreshold) //is the distance is greater than the threshhold than update zombie positions || or the rotation is greater than the threshold
        {
            lastPos = myTransform.position; //last position set
            lastRot = myTransform.rotation; //last rotation set

            //SyncVars updating on the server, based on the last positions
            syncPos = myTransform.position;
            syncYRot = myTransform.localEulerAngles.y;
        }
    }


    //client funntion to lerp the pos&rot
    void LerpMotion()
    {
        //only hapenning on the client
        if (isServer)
        {
            return;
        }

        //do on client
        myTransform.position = Vector3.Lerp(myTransform.position, syncPos, Time.deltaTime * lerpRate); //lerp from current position to the updated SyncVar position

        Vector3 newRot = new Vector3(0, syncYRot, 0);

        myTransform.rotation = Quaternion.Lerp(myTransform.rotation, Quaternion.Euler(newRot), Time.deltaTime * lerpRate); //lerp from the current rotation to the updated SyncVar rotation
    }
}
