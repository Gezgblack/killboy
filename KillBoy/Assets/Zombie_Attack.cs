﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class Zombie_Attack : NetworkBehaviour {

    private float attackRate = 1; //atacking rate
    private float nextAttack; //next attack
    public int damage = 10; //damage amount
    private float minDistance = 8.5f; 
    private float currentDistance;
    private Transform myTransform;
    private Zombie_Target targetScript;

    [SerializeField]
    private Material zombieGreen; //non attack material
    [SerializeField]
    private Material zombieRed; //attack material


	// Use this for initialization
	void Start () {

        myTransform = transform;
        targetScript = GetComponent<Zombie_Target>();

        if (isServer) //only run on server and not clients
        {
            StartCoroutine(Attack());
        }
	}

    void CheckIfTargetInRange() //check if there is a target
    {
        if(targetScript.targetTransform != null) //if targ transofrm not null DO
        {
            currentDistance = Vector3.Distance(targetScript.targetTransform.position, myTransform.position); //distance between the 2

            if(currentDistance < minDistance && Time.time > nextAttack) //if attacking condition satisfied and zombie within distance
            {
                nextAttack = Time.time + attackRate; //next attack set

                //deduct health
                targetScript.targetTransform.GetComponent<Player_Health>().DeductHealth(damage);

                //change zombie material
                //StartCoroutine(ChangeZombieMat()); //for the host player
                RpcChangeZombieAppearance();
            }
        }
    }

    //IEnumerator ChangeZombieMat()
    //{
        //GetComponent<Renderer>().material = zombieRed; //zombie hitting material (red)
        //yield return new WaitForSeconds(attackRate/2); //change within the attacking rate
        //GetComponent<Renderer>().material = zombieGreen; //set back to the default persue materail (green)
    //}

    [ClientRpc] //to run on the clients
    void RpcChangeZombieAppearance()
    {
        //StartCoroutine(ChangeZombieMat());
    }

    IEnumerator Attack()
    {
        for (;;) //endless for loop
        {
            yield return new WaitForSeconds(0.2f); //every 0.2 seconds
            CheckIfTargetInRange();
        }
    }
}
