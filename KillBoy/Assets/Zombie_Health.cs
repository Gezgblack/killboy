﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class Zombie_Health : NetworkBehaviour { //Zombie health controls, only called on the server

    public int health = 250; //zombie health

    public void DeductHealth(int dmg) //make damage
    {
        health -= dmg; //take damage amount away from health
        CheckHealth();
    }

    void CheckHealth()
    {
        if(health <= 0)
        {
            Destroy(gameObject);
        }
    }
}
