﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Networking;

public class SpawnManager_waveLevel : NetworkBehaviour {

    [SerializeField]
    public Text waveText;

    [SyncVar(hook = "OnLevelChange")]
    public int waveCounter = 0;

    public override void OnStartClient()
    {
        OnLevelChange(waveCounter);
    }

    private void OnLevelChange(int newValue)
    {
        //waveText = GameObject.Find("levelwaveManager").GetComponent<SpawnManager_waveLevel>();
        waveText.text = newValue.ToString();
    }

}
