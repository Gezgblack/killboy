﻿using UnityEngine;
using System.Collections;

public class scoreUpdater : MonoBehaviour {

    highScores hs;

	// Use this for initialization
	void Start () {

        hs = GetComponent<highScores>();

        int score = PlayerPrefs.GetInt("playerScore");
        string email = PlayerPrefs.GetString("email");
        hs.AddNewHighScore(email, score);
    }
	
	// Update is called once per frame
	void Update () {

        
	}
}
