﻿using UnityEngine;
using System.Collections;

public class difficultyManager : MonoBehaviour { // this class constrols all the difficulty settings

    zombieController zombieControls; // the speed of the enemy, the detection time, etc..
    enemyHealth healthControls; // the amount of health on the enemy, enemies damage, etc..
    enemyDamage enemyDamageControls;
    BoxCollider detectionDistance;
    string chooser = "";
    
	// Use this for initialization
	void Start () { //find componenets

        zombieControls = GetComponentInChildren<zombieController>();
        healthControls = GetComponent<enemyHealth>();
        enemyDamageControls = GetComponentInChildren<enemyDamage>();
        detectionDistance = GetComponentInChildren<BoxCollider>();
        chooser = PlayerPrefs.GetString("gameDifficulty");
        difficultyChooser();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void difficultyChooser() //to choose the different difficulty specifications
    {
        if(chooser == "Easy")
        {
            zombieControls.runSpeed = 250f; // Run Speed
            zombieControls.walkSpeed = 100f; // Walk Speed
            healthControls.enemyMaxHealth = 15; // Health Amount
            enemyDamageControls.damage = 5; // Damage Amount
            detectionDistance.size = new Vector3(1.02f, 0.83f, 9f); //Detection Zone
        }
        else if(chooser == "Normal")
        {
            zombieControls.runSpeed = 300f;
            zombieControls.walkSpeed = 120f;
            healthControls.enemyMaxHealth = 25;
            enemyDamageControls.damage = 10;
            detectionDistance.size = new Vector3(1.02f, 0.83f, 11f);
        }
        else if (chooser == "Hard")
        {
            zombieControls.runSpeed = 350f;
            zombieControls.walkSpeed = 150f;
            healthControls.enemyMaxHealth = 30;
            enemyDamageControls.damage = 15;
            detectionDistance.size = new Vector3(1.02f, 0.83f, 15f);
        }
    }
}
