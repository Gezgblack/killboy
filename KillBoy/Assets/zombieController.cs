﻿using UnityEngine;
using System.Collections;

public class zombieController : MonoBehaviour { //in control of what the zombie does

    public GameObject flipModel; //part of the zombie we flip based direction

    //Sounds
    public AudioClip[] idleSounds; //idle sounds
    public float idleSoundTime; //how often the sound is played
    AudioSource enemyMovementAS;
    float nextIdleSound = 0f; //imediate

    //detection
    public float detectionTime; //the time the player has to be within the detection zone to get aggrod
    float startRun; //after detection time, the run time
    bool firstDetection; //detected or not

    //movements
    public float runSpeed; //how fast zombie can run
    public float walkSpeed; //how fast zombies walk
    public bool facingRight = true; //zombies spawn facing right

    float moveSpeed;
    bool running; //true if zombie running

    Rigidbody myRb;
    Animator myAnim;
    Transform detectedPlayer; //where the player is with relation to zombie

    bool Detected; //once player is detected activate

	// Use this for initialization
	void Start () {

        //Find
        myRb = GetComponentInParent<Rigidbody>();
        myAnim = GetComponentInParent<Animator>();
        enemyMovementAS = GetComponent<AudioSource>();

        running = false;
        Detected = false;
        firstDetection = false;
        moveSpeed = walkSpeed;

        if (Random.Range(0, 10) > 5) Flip();
	}
	
	// Update is called once per frame
	void FixedUpdate () {

        if (Detected)
        {
            if (detectedPlayer.position.x < transform.position.x && facingRight) Flip(); //facing right
            else if (detectedPlayer.position.x > transform.position.x && !facingRight) Flip(); //facing left

            if (!firstDetection) //if not initial detect
            {
                startRun = Time.time + detectionTime;
                firstDetection = true;
            }
        }

        if (Detected && !facingRight) myRb.velocity = new Vector3((moveSpeed * -1), myRb.velocity.y, 0); //to flip left -1
        else if(Detected && facingRight) myRb.velocity = new Vector3((moveSpeed), myRb.velocity.y, 0);

        if(!running && Detected) //if we zombie deteted and not running start
        {
            if(startRun < Time.time) //if walking
            {
                moveSpeed = runSpeed; //change from walk to run
                myAnim.SetTrigger("run"); //trigger animation
                running = true; //change status of running
            }
        }

        if (!running)
        {
            if(Random.Range(0,10)>5 && nextIdleSound < Time.time)
            {
                AudioClip tempClip = idleSounds[Random.Range(0, idleSounds.Length)]; //random zombie idle sound
                enemyMovementAS.clip = tempClip;
                enemyMovementAS.Play(); //play the random clip
                nextIdleSound = idleSoundTime + Time.time;
            }
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player" && !Detected) //if collider is player and we are not detected
        {
            Detected = true; //set Detected
            detectedPlayer = other.transform; //where our player is
            myAnim.SetBool("detected", Detected); //animation play

            if (detectedPlayer.position.x < transform.position.x && facingRight) Flip(); //facing right
            else if (detectedPlayer.position.x > transform.position.x && !facingRight) Flip(); //facing left
        }
    }

    void OnTriggerExit(Collider other)
    {
        if(other.tag == "Player")
        {
            firstDetection = false; //player no longer in range

            if (running)
            {
                myAnim.SetTrigger("run"); //from running to walking
                moveSpeed = walkSpeed; //change the speed of the animation to the walk speed
                running = false;
            }
        }
    }

    void Flip()
    {
        facingRight = !facingRight; //invertion
        Vector3 theScale = flipModel.transform.localScale;
        theScale.z *= -1; //flip on the Z axis
        flipModel.transform.localScale = theScale;
    }
}
