﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.UI;

public class Player_Health : NetworkBehaviour {

    [SyncVar(hook ="OnHealthChanged")] //hooked to OnHealthChanged which will change the value
    private int health = 100;
    private int fullHealth = 100;
    private Text healthText;
    private Slider healthSlider;
    private bool shouldDie = false;
    public bool isDead = false;

    private Player_Shoot money;

    public delegate void DieDelegate();
    public event DieDelegate EventDie; //our die event

    public override void OnStartLocalPlayer()
    {
        healthSlider = GameObject.Find("playerHealthSlider").GetComponent<Slider>(); //find our health slider
        healthSlider.value = health;
        healthText = GameObject.Find("HealthText").GetComponent<Text>(); //find our health text
        SetHealthText();
        money = GetComponent<Player_Shoot>();
    }

	
	// Update is called once per frame
	void Update () {

        CheckCondition();
	}

    void CheckCondition() //checking for the death conditions
    {
        if(health <=0 && !shouldDie && !isDead) //if health is less then or equal to 0 and both conditions are not true
        {
            shouldDie = true; //the player should be die
        }

        if(health <=0 && shouldDie) //if health 0 or less and shouldDie true
        {
            if(EventDie != null)
            {
                EventDie(); //trigger event
            }

            shouldDie = false; //back to default
        }
    }

    void SetHealthText()
    {
        if (isLocalPlayer) //only done by the local player
        {
            healthText.text = "Health " + health.ToString();
            healthSlider.value = health;
        }
    }

    public void DeductHealth(int dmg) //to reduce player health
    {
        health -= dmg; //take away damage amount
    }

    void OnHealthChanged(int he) //hook function to be used with the syncvar
    {
        health = he;
        SetHealthText();
    }

    /*ADD HEALTH*/
    public void AddHealth() //buying health
    {
        health = fullHealth; //set remaining health to the max amount
        healthSlider.value = health; //update the slider
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Health" && health < fullHealth) //&& money.playerMoney >= 5) //if he player layer is colliding with the collider and the health is not full 
        {
            AddHealth(); //Buy the health
            money.playerMoney -= 5;
        }
    }
}
